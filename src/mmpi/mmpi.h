#ifndef META_MPI_H
#define META_MPI_H

#include <string>
#include <typeinfo>
#include <sstream>
#include <stdexcept>
using namespace std;
#include <time.h>
#include <unistd.h>
#include <mpi.h>

/**
 * Meta MPI is a wrapper for MPI C++. It simplifies the use
 * of MPI send, receive, gather, scatter functions.
 * For the gather, scatter and reduce functions the processor
 * of rank 0 is considered as the "master" that collects or
 * sends data.
 * This class also acts as a logger so it can be used to print
 * information
 */
class MMPI {
protected:
    // maximum number of processors
    int m_max_cpus;
    // identifier of current processor, called rank for MPI
    int m_cpu_rank;
    // identifier of remote processor for send, receive, ...
    int m_remote_cpu;
    // status of last operation
    MPI::Status m_status;
    // message tag if needed (default is 0)
    int m_message_tag;
    // name of processor
    string m_cpu_name;
    // processus identifier
    int m_pid;
    // verbose mode
    bool m_verbose;
    // main output stream for current processor
    ostringstream log;
    // temporary output stream
    ostringstream tmp_log;

private:
    /**
     * Find processor name
     */
    void find_cpu_name();

    /**
     * record output of oss into general output
     */
    void append();

    /**
     * initialize max_cpus, cpu_rank, processus id
     */
    void init();

public:

    /**
     * Default constructor
     */
    MMPI();

    /**
     * Display output of each processor if verbose mode is on
     */
    void finalize();

    /**
     * set verbose mode
     */
    void verbose(bool mode) {
        m_verbose = mode;
    }

    /**
     * Get process identifier
     */
    int pid();

    /**
     * Get processor identifier or rank
     */
    int cpu_rank();

    /**
     * Get number of processors used
     */
    int max_cpus();

    /**
     * Get processor name
     */
    string cpu_name();

    /**
     * set remote processor identifier
     */
    void remote(int rmt);

    /**
     * set message tag
     * @param tag must be an integer between 0 and 32767
     */
    void tag(int tag);

    /**
     * Return true if this processor is the processor of rank 0
     * considered as the master.
     */
    bool is_master() {
        return (m_cpu_rank == 0);
    }

    /**
     * synchronize
     */
    void barrier();

    /**
     * Determine type of data T and convert it into MPI::Datatype.
     * This function needs to be extended with other types.
     */
    template<class T>
    MPI::Datatype get_type() {
        if (typeid(T) == typeid(char)) {
            return MPI::CHAR;
        } else if (typeid(T) == typeid(int)) {
            return MPI::INT;
        } else if (typeid(T) == typeid(float)) {
            return MPI::FLOAT;
        } else if (typeid(T) == typeid(double)) {
            return MPI::DOUBLE;
        }
        throw std::runtime_error("unknown MPI::Datatype");
        return MPI::INT;
    }

    /**
     * Send one instance of data to remote_cpu
     * @param v data to send
     */
    template<class T>
    void send(T& v) {
        MPI::Datatype data_type = get_type<T>();

        if (m_verbose) {
            tmp_log << "send value=" << v;
            print_ln();
        }

        MPI::COMM_WORLD.Send(&v, 1, data_type, m_remote_cpu, m_message_tag);
    }

    /**
     * Send an array to remote_cpu
     * @param arr address of the array
     * @param size number of elements to send
     */
    template<class T>
    void send(T *arr, int size) {
        MPI::Datatype data_type = get_type<T>();

        if (m_verbose) {
            tmp_log << "send array of size=" << size;
            print_ln();
        }

        MPI::COMM_WORLD.Send(&arr[0], size, data_type, m_remote_cpu, m_message_tag);
    }

    /**
     * Receive one instance of data from remote cpu
     * @param v data to receive
     */
    template<class T>
    void recv(T& v) {
        MPI::Datatype data_type = get_type<T>();

        MPI::COMM_WORLD.Recv(&v, 1, data_type, m_remote_cpu,
                             (m_message_tag == 0) ? MPI::ANY_TAG : m_message_tag,
                             m_status);

        if (m_verbose) {
            tmp_log << "receive value=" << v;
            print_ln();
        }
    }

    /**
     * Receive an array of given size
     * @param arr pointer to address of the array
     * @param size number of elements
     */
    template<class T>
    void recv(T *arr, int size) {
        MPI::Datatype data_type = get_type<T>();

        MPI::COMM_WORLD.Recv(&arr[0], size, data_type, m_remote_cpu,
                             (m_message_tag == 0) ? MPI::ANY_TAG : m_message_tag,
                             m_status);

        if (m_verbose) {
            tmp_log << "receive array of size=" << size;
            print_ln();
        }
    }

    /**
     * Send array and receive value in return, this is an instance
     * of the Sendrecv function.
     * @param arr address of the array to send
     * @param size size of the array to send
     * @param value value to receive
     */
    template<class T, class U>
    void sendrecv(T *array, int size, U& value) {
        MPI::Datatype array_data_type = get_type<T>();
        MPI::Datatype value_data_type = get_type<U>();

        if (m_verbose) {
            tmp_log << "sendrecv/send array of size=" << size;
            print_ln();
        }

        MPI::COMM_WORLD.Sendrecv(&array[0], size, array_data_type, m_remote_cpu, 0,
                                 &value, 1, value_data_type, MPI::ANY_SOURCE, MPI::ANY_TAG,
                                 m_status);

        if (m_verbose) {
            tmp_log << "sendrecv/receive value=" << value;
            print_ln();
        }
    }

    /**
     * Perform reduction
     * @param lcl_value local array used to perform reduction
     * @param glb_value global data that will contain result
     * @param op operation to perform (MPI::SUM, MPI::MAX, ...)
     */
    template<class T>
    void reduce(T &lcl_value, T &glb_value, const MPI::Op& op) {
        MPI::Datatype data_type = get_type<T>();

        MPI::COMM_WORLD.Reduce(&lcl_value,
                               &glb_value, 1, data_type, op, 0);

        if (m_verbose) {
            tmp_log << "reduction gives value=" << glb_value;
            print_ln();
        }
    }

    /**
     * Perform gather operation
     * @param lcl_array local array that is send to master process
     * @param glb_array global array that will contain all local arrays
     */
    template<class T>
    void gather(T *lcl_array, int size, T *glb_array) {
        MPI::Datatype data_type = get_type<T>();

        MPI::COMM_WORLD.Gather(lcl_array, size, data_type,
                               glb_array, size, data_type, 0);

        if (m_verbose) {
            tmp_log << "gather";
            print_ln();
        }
    }

    /**
     * Perform scatter operation
     * @param glb_array array of data that will be send by to all processors
     * @param size size of the local array of data
     * @param lcl_array local array of data
     */
    template<class T>
    void scatter(T *glb_array, int size, T *lcl_array) {
        MPI::Datatype data_type = get_type<T>();

        MPI::COMM_WORLD.Scatter(glb_array, size, data_type,
                                lcl_array, size, data_type, 0);

        if (m_verbose) {
            tmp_log << "scatter";
            print_ln();
        }
    }

    /**
     * Add temporary log to general log.
     * Function called when MMPI::endl is invoked.
     */
    void print_ln();

    void print(char v);
    void print(int v);
    void print(string s);
    void print(float f);
    void print(double d);

    /**
     * definition of MMPI::endl that will record message
     */
    static MMPI& endl(MMPI &l) {
        l.print_ln();
        return l;
    }

    /**
     * redefinition of operator << in order to be able to call endl
     */
    MMPI& operator<<(MMPI& (*m)(MMPI&)) {
        return (*m)(*this);
    }

    template<class T>
    MMPI& operator<<(const T& a) {
        print(a);
        return *this;
    }

};

#endif