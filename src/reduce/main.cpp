#include <algorithm>
#include "../mmpi/mmpi.h"
#include "../utils/repartition_algorithm.h"
#include "../utils/utils.h"
#include "../utils/ansi_colors.h"

void scatterArray(MMPI &mmpi, int *array, std::vector<ThreadPosition> &positions) {
    // Send array parts
    for (int i = 1; i < mmpi.max_cpus(); ++i) {
        mmpi.remote(i);
        mmpi.send(positions[i].size); //Send size of the array so slaves can allocate memory
        mmpi.send(array + positions[i].start, positions[i].size); //Send array
    }
}


int main(int argc, char ** argv) {

    MPI::Init(argc, argv);
    MMPI mmpi;

    int localMin;

    int *global_data = nullptr;
    int global_data_size = 10;
    bool verbose = false;
    int opt;


    while ((opt = getopt(argc, argv, "s:v")) != -1) {
        switch (opt) {
            case 's':
                global_data_size = atoi(optarg);
                break;
            case 'v':
                verbose = true;
                break;
            default: /* aq?aq */
                fprintf(stderr, "Usage: %s [-s size]",
                        argv[0]);
                exit(EXIT_FAILURE);
        }
    }


    if(mmpi.is_master()){


        global_data = new int[global_data_size];

        populateArrayRandom(global_data, global_data_size, 0, 10);

        if(verbose) logArray(global_data, global_data_size, mmpi, "global array=", "");

        //Compute repartition of the array
        std::vector<ThreadPosition> positions(static_cast<unsigned long>(mmpi.max_cpus()));

        equalRepartition(positions, global_data_size, mmpi.max_cpus());

        scatterArray(mmpi, global_data, positions);

        //Find min of first part of the array

        localMin = *(std::min_element(global_data, global_data + positions[0].size));

    }else{
        int *local_data;

        mmpi.remote(0);

        int local_data_size;
        //Receive size of the array
        mmpi.recv(local_data_size);
        // each processor creates its local data
        local_data = new int[local_data_size];

        //Receive array
        mmpi.recv(local_data, local_data_size);

        localMin = *(std::min_element(local_data, local_data + local_data_size));

        delete[] local_data;
    }

    MPI::COMM_WORLD.Barrier();

    int finalResult;

    mmpi << "local_min=" << localMin << MMPI::endl;
    mmpi.reduce(localMin, finalResult, MPI::MIN);

    if(mmpi.is_master()){
        mmpi << "Final Result " << finalResult << MMPI::endl;
        int expected = *std::min_element(global_data, global_data + global_data_size);
        if(expected == finalResult){
            mmpi << ANSI_COLOR_GREEN << "Result ok" << ANSI_COLOR_RESET << MMPI::endl;
        }else{
            mmpi << ANSI_COLOR_RED << "Erreur : got " << finalResult << ", expected " << expected << ANSI_COLOR_RESET << MMPI::endl;
        }

        delete[] global_data;
    }

    mmpi.finalize();
    MPI::Finalize();


    return 0;
}

