//
// Created by etudiant on 09/11/18.
//

#include <iostream>
#include <algorithm>

#include "../mmpi/mmpi.h"
#include "../utils/utils.h"
#include "../utils/ansi_colors.h"

int main(int argc, char ** argv) {

    MPI::Init(argc, argv);
    MMPI mmpi;

    int local_data_size = 10;
    int opt;
    bool verbose = false;
    while ((opt = getopt(argc, argv, "s:v")) != -1) {
        switch (opt) {
            case 's':
                local_data_size = atoi(optarg);
                break;
            case 'v':
                verbose = true;
                break;
            default: /* aq?aq */
                fprintf(stderr, "Usage: %s [-s size]",
                        argv[0]);
                exit(EXIT_FAILURE);
        }
    }


    int * local_data = new int[local_data_size];

    int global_data_size = mmpi.max_cpus() * local_data_size;
    int * global_data = nullptr;

    if(mmpi.is_master()){
        global_data =  new int[global_data_size];

        populateArrayRandom(global_data, global_data_size, 0, 100);

        if(verbose) logArray(global_data, global_data_size, mmpi, "global_data=", "");
    }

    mmpi.scatter(global_data, local_data_size, local_data);

    int localMin = * (std::min_element(local_data, local_data + local_data_size));

    MPI::COMM_WORLD.Barrier();

    int finalResult;

    if(verbose) logArray(local_data, local_data_size, mmpi, "local_data=", "");
    mmpi << "local_min=" << localMin << MMPI::endl;
    mmpi.reduce(localMin, finalResult, MPI::MIN);

    if(mmpi.is_master()){
        mmpi << "final_result=" << finalResult << MMPI::endl;
        //Check
        int expected = *std::min_element(global_data, global_data + global_data_size);
        if(expected == finalResult){
            mmpi << ANSI_COLOR_GREEN << "Result ok" << ANSI_COLOR_RESET << MMPI::endl;
        }else{
            mmpi << ANSI_COLOR_RED << "Erreur : got " << finalResult << ", expected " << expected << ANSI_COLOR_RESET << MMPI::endl;
        }
    }

    mmpi.finalize();
    MPI::Finalize();


    return 0;
}

