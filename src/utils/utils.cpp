//
// Created by etudiant on 08/11/18.
//

#include <random>
#include "utils.h"

bool isSorted(const int * array, int size){
    for(int i = 0; i < size - 1; ++i ){
        if(array[i] > array[i + 1]){
            return false;
        }
    }
    return true;
}

void populateArrayRandom(int *array, int size, int min, int max) {
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<int> dist(min, max);

    for (int i = 0; i < size; ++i) array[i] = dist(mt);
}

void populateArrayRandom(int *array, int size) {
    populateArrayRandom(array, size, 1, 10000);
}


void logArray(int *array, int size, MMPI &mmpi, const char *prefix, const char *suffix) {
    mmpi << prefix << "[";
    for (int i = 0; i < size; ++i) {
        mmpi << array[i] << " ";
    }
    mmpi << "]" << suffix << MMPI::endl;;
}